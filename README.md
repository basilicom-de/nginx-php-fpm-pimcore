# Customized nginx:latest Docker Image for Pimcore 5

## Notes

* the image expects pimcore at `/php` - mount or copy your code!
* document root is at default pimcore 5 symfony location: `/php/web`
* this image is optimized to be used with `basilicom/php-7-fpm-pimcore`
